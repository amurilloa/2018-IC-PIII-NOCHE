﻿using DemoCapasBOL;
using DemoCapasENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoCapas
{
    public partial class FrmLogin : Form
    {
        private UsuarioBOL ubo;

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            ubo = new UsuarioBOL();
        }

        private void IniciarSesion(object sender, EventArgs e)
        {
            EUsuario usu = new EUsuario
            {
                Usuario = txtUsuario.Text.Trim(),
                Email = txtUsuario.Text.Trim(),
                Password = txtPass.Text.Trim()
            };

            usu = ubo.Login(usu);
            if (usu != null)
            {
                FrmMantUsuarios frm = new FrmMantUsuarios();
                Hide();
                frm.Show(this);
                //LimpiarInterfaz();
            }
            else
            {
                MessageBox.Show("Usuario/Contraseña inválidos.");
            }
        }

        private void LimpiarInterfaz()
        {
            txtUsuario.Text = "username";
            txtPass.Text = "password";
        }
    }
}
