﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DemoCapasBOL;
using DemoCapasENL;

namespace DemoCapas
{
    public partial class FrmMantUsuarios : Form
    {
        private EUsuario usuario;
        private UsuarioBOL ubo;

        public FrmMantUsuarios()
        {
            InitializeComponent();
        }

        private void FrmMantUsuarios_Load(object sender, EventArgs e)
        {
            usuario = new EUsuario();
            ubo = new UsuarioBOL();
        }
        private void CargarUsuarios_Click(object sender, EventArgs e)
        {
            CargarUsuarios(txtFiltro.Text.Trim());
        }

        /// <summary>
        /// Carga los usuarios en el grid
        /// </summary>
        private void CargarUsuarios(string filtro)
        {
            dgvUsuarios.DataSource = ubo.CargarUsuarios(filtro);
        }

        private void Nuevo_Click(object sender, EventArgs e)
        {
            usuario = new EUsuario();
            CargarDatos();
        }

        private void FrmMantUsuarios_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
        }

        /// <summary>
        /// Carga los datos del usuario en la interfaz
        /// </summary>
        private void CargarDatos()
        {
            txtCed.Text = usuario.Cedula;
            txtNom.Text = usuario.Nombre;
            txtPrim.Text = usuario.ApeUno;
            txtSegun.Text = usuario.ApeDos;
            txtEmail.Text = usuario.Email;
            txtUser.Text = usuario.Usuario;
            txtPass.Text = usuario.Password;
            pbFoto.Image = usuario?.Foto?.Foto;
            pbFoto.Image = pbFoto.Image == null ? pbFoto.Image = Properties.Resources.profile : pbFoto.Image;
        }

        private void Guardar_Click(object sender, EventArgs e)
        {
            try
            {
                usuario.Cedula = txtCed.Text.Trim();
                usuario.Nombre = txtNom.Text.Trim();
                usuario.ApeUno = txtPrim.Text.Trim();
                usuario.ApeDos = txtSegun.Text.Trim();
                usuario.Email = txtEmail.Text.Trim();
                usuario.Usuario = txtUser.Text.Trim();
                usuario.Password = txtPass.Text.Trim();
                MessageBox.Show(ubo.Guardar(usuario, txtPassRe.Text.Trim()) ? "Guardado con éxito" : "Favor intente nuevamente");
                CargarUsuarios(txtFiltro.Text.Trim());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void Seleccionar_Click(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgvUsuarios.SelectedRows[0].Index;
            usuario = new EUsuario
            {
                Id = Convert.ToInt32(dgvUsuarios["idCol", row].Value),
                Cedula = dgvUsuarios["cedCol", row].Value.ToString(),
                Nombre = dgvUsuarios["nomCol", row].Value.ToString(),
                ApeUno = dgvUsuarios["apuCol", row].Value.ToString(),
                ApeDos = dgvUsuarios["apsCol", row].Value.ToString(),
                Usuario = dgvUsuarios["usuCol", row].Value.ToString(),
                Email = dgvUsuarios["emaCol", row].Value.ToString(),
                Password = dgvUsuarios["pasCol", row].Value.ToString(),
                Foto = (EFoto)dgvUsuarios["fotCol",row].Value
            };
            CargarDatos();
        }

        private void Eliminar_Click(object sender, EventArgs e)
        {
            if (usuario.Id > 0)
            {
                DialogResult result =
                    MessageBox.Show(String.Format("Esta seguro que desea eliminar a {0}",
                    usuario.Usuario), "Eliminar", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    ubo.Borrar(usuario);
                    CargarUsuarios(txtFiltro.Text.Trim());
                    usuario = new EUsuario();
                    CargarDatos();
                }

            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            if (txtFiltro.Text.Length >= 3)
            {
                CargarUsuarios(txtFiltro.Text.Trim());
            }
            else {
                dgvUsuarios.DataSource = null;
            }
        }

        private void Foto_Click(object sender, EventArgs e)
        {
            if (fdFoto.ShowDialog() == DialogResult.OK)
            {
                if (usuario.Foto == null) {
                    usuario.Foto = new EFoto { Foto = Image.FromFile(fdFoto.FileName) };
                }
                else {
                    usuario.Foto.Foto = Image.FromFile(fdFoto.FileName);
                }
                pbFoto.Image = usuario.Foto.Foto;
            }
        }
    }
}
