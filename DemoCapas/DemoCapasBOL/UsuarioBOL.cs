﻿using DemoCapasDAL;
using DemoCapasENL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoCapasBOL
{
    public class UsuarioBOL
    {
        private UsuarioDAL uda;

        public UsuarioBOL()
        {
            uda = new UsuarioDAL();
        }

        public List<EUsuario> CargarUsuarios(string filtro)
        {
            //Validar todo lo que sea necesario. 
            Console.WriteLine("2. BOL");
            return uda.CargarTodo(filtro);

        }

        public EUsuario Login(EUsuario usu)
        {
            if (String.IsNullOrEmpty(usu.Usuario) || String.IsNullOrEmpty(usu.Email))
            {
                return null;
            }
            if (String.IsNullOrEmpty(usu.Password))
            {
                return null;
            }
            //Fortaleza de la contraseña
            if (!VerificarPass(usu.Password))
            {
                return null;
            }

            return uda.Verificar(usu);
        }

        public bool Guardar(EUsuario usuario, string repass)
        {
            if (String.IsNullOrEmpty(usuario.Nombre)
                || String.IsNullOrEmpty(usuario.ApeUno)
                || String.IsNullOrEmpty(usuario.ApeDos)
                || String.IsNullOrEmpty(usuario.Cedula))
            {
                throw new Exception("Datos personales requeridos");
            }
            if (String.IsNullOrEmpty(usuario.Usuario)
                || String.IsNullOrEmpty(usuario.Email)
                || String.IsNullOrEmpty(usuario.Password))
            {
                throw new Exception("Datos de usuario requeridos");
            }

            //TODO: Validar formato del Email

            if (!VerificarPass(usuario.Password))
            {
                throw new Exception("Constraseña no cumple con las normas de seguridad");
            }

            if (!usuario.Password.Equals(repass))
            {
                throw new Exception("Contraseñas no coinciden");
            }
            if (usuario.Id > 0)
            {
                return uda.Modificar(usuario);
            }
            else
            {
                return uda.Insertar(usuario);
            }
        }

        private bool VerificarPass(string password)
        {
            return password.Length > 8;
        }

        public void Borrar(EUsuario usuario)
        {
            if (usuario.Id > 0)
            {
                uda.Eliminar(usuario);
            }
        }
    }
}
