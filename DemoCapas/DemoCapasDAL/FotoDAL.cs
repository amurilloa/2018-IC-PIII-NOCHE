﻿using DemoCapasENL;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoCapasDAL
{
    public class FotoDAL
    {
        public EFoto Insertar(EFoto foto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"insert into foto(foto) values (@foto) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                foto.Foto.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@foto", pic);

                foto.Id = Convert.ToInt32(cmd.ExecuteScalar());

                return foto;
            }
        }

        public EFoto FotoId(int idFoto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                EFoto foto = new EFoto();
                con.Open();
                string sql = @"select id, foto
                               from foto where id = @id  order by id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", idFoto);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarFoto(reader);
                }

                return foto;
            }
        }

        private EFoto CargarFoto(NpgsqlDataReader reader)
        {
            EFoto foto = new EFoto();
            foto.Id = Convert.ToInt32(reader["id"]);
            byte[] f = new byte[0];
            f = (byte[])reader["foto"];
            MemoryStream stream = new MemoryStream(f);
            foto.Foto = Image.FromStream(stream);
            return foto;
        }

        public void Modificar(EFoto foto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"UPDATE foto
	                            SET foto = @foto
	                            WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                foto.Foto.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@foto", pic);

                cmd.Parameters.AddWithValue("@id", foto.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public void Eliminar(EFoto foto, NpgsqlCommand cmd = null, NpgsqlTransaction tr = null)
        {
            string sql = @"DELETE FROM foto WHERE id = @id";
            if (cmd != null && tr != null)
            {
                cmd.CommandText = sql;
                cmd.Transaction = tr;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", foto.Id);
                cmd.ExecuteNonQuery();
            }
            else
            {
                using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
                {
                    con.Open();
                    cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id", foto.Id);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
