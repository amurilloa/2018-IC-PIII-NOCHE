﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapasENL;
using Npgsql;

namespace DemoCapasDAL
{
    public class UsuarioDAL
    {
        public List<EUsuario> CargarTodo(string filtro)
        {

            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                List<EUsuario> usuarios = new List<EUsuario>();
                con.Open();
                string sql = @"select id, cedula, usuario, nombre, 
	                             apellido_uno, apellido_dos, pass, email, id_foto
                               from usuario order by usuario";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                if (!String.IsNullOrEmpty(filtro))
                {
                    sql = @"select id, cedula, usuario, nombre, 
	                           apellido_uno, apellido_dos, pass, email, id_foto 
                               from usuario where lower(cedula) like @filtro 
                               or lower(nombre) like @filtro or lower(apellido_uno) like @filtro
                               or lower(usuario) like @filtro or lower(email) like @filtro 
                               order by usuario";
                    cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@filtro", filtro.ToLower() + "%");

                }
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    usuarios.Add(CargarUsuario(reader));
                }

                return usuarios;
            }

        }

        private EUsuario CargarUsuario(NpgsqlDataReader reader)
        {
            EUsuario usu = new EUsuario();
            //TODO: No es necesario validar el id, es solo un ejemplo
            usu.Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0;
            usu.Cedula = reader["cedula"].ToString();
            usu.Usuario = reader["usuario"].ToString();
            usu.Nombre = reader["nombre"].ToString();
            usu.ApeUno = reader["apellido_uno"].ToString();
            usu.ApeDos = reader["apellido_dos"].ToString();
            usu.Password = reader["pass"].ToString();
            usu.Email = reader["email"].ToString();
            FotoDAL fdal = new FotoDAL();
            int idFoto = reader["id_foto"] != DBNull.Value ? Convert.ToInt32(reader["id_foto"]) : 0;
            usu.Foto = idFoto == 0 ? new EFoto() : fdal.FotoId(idFoto);
            return usu;
        }

        public EUsuario Verificar(EUsuario usu)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"select id, cedula, usuario, nombre, 
	                             apellido_uno, apellido_dos, pass, email, id_foto
                               from usuario where (usuario = @usuario or email=@email) and 
                               pass = @pass";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usuario", usu.Usuario);
                cmd.Parameters.AddWithValue("@email", usu.Email);
                cmd.Parameters.AddWithValue("@pass", usu.Password);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
            }
            return null;
        }

        public void Eliminar(EUsuario usuario)
        {
            NpgsqlTransaction tr = null;
            NpgsqlConnection con = null;
            try
            {
                con = new NpgsqlConnection(Configuracion.ConStr);
                con.Open();
                tr = con.BeginTransaction();
                string sql = @"DELETE FROM usuario WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Transaction = tr;
                cmd.Parameters.AddWithValue("@id", usuario.Id);
                cmd.ExecuteNonQuery();

                FotoDAL fdal = new FotoDAL();
                if (usuario.Foto != null && usuario.Foto.Id > 0)
                {
                    fdal.Eliminar(usuario.Foto, cmd, tr);
                }

                tr.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (tr != null) { tr.Rollback(); }
                throw;
            }
        }

        public bool Insertar(EUsuario usu)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO usuario(cedula, usuario, nombre, apellido_uno, apellido_dos, pass, email, id_foto)
	                            VALUES (@ced, @usu, @nom, @auno, @ados, @pas, @ema, @id_foto);";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", usu.Cedula);
                cmd.Parameters.AddWithValue("@usu", usu.Usuario);
                cmd.Parameters.AddWithValue("@nom", usu.Nombre);
                cmd.Parameters.AddWithValue("@auno", usu.ApeUno);
                cmd.Parameters.AddWithValue("@ados", usu.ApeDos);
                cmd.Parameters.AddWithValue("@pas", usu.Password);
                cmd.Parameters.AddWithValue("@ema", usu.Email);

                //Insertar la foto
                if (usu.Foto != null && usu.Foto.Foto != null)
                {
                    FotoDAL fdal = new FotoDAL();
                    fdal.Insertar(usu.Foto);
                }

                if (usu.Foto?.Id > 0)
                {
                    cmd.Parameters.AddWithValue("@id_foto", usu.Foto.Id);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@id_foto", DBNull.Value);
                }
                return cmd.ExecuteNonQuery() > 0;
            }
        }

        public bool Modificar(EUsuario usu)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"UPDATE usuario
	                            SET cedula=@ced, usuario=@usu, nombre=@nom, 
	                                apellido_uno=@auno, apellido_dos=@ados, 
		                            pass=@pas, email=@ema, id_foto = @fot
	                            WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", usu.Cedula);
                cmd.Parameters.AddWithValue("@usu", usu.Usuario);
                cmd.Parameters.AddWithValue("@nom", usu.Nombre);
                cmd.Parameters.AddWithValue("@auno", usu.ApeUno);
                cmd.Parameters.AddWithValue("@ados", usu.ApeDos);
                cmd.Parameters.AddWithValue("@pas", usu.Password);
                cmd.Parameters.AddWithValue("@ema", usu.Email);
                cmd.Parameters.AddWithValue("@id", usu.Id);

                FotoDAL fdal = new FotoDAL();
                if (usu?.Foto?.Id == 0 && usu?.Foto?.Foto != null)
                {
                    usu.Foto = fdal.Insertar(usu.Foto);
                    cmd.Parameters.AddWithValue("@fot", usu.Foto.Id);
                }
                else if (usu.Foto != null && usu.Foto.Id > 0)
                {
                    fdal.Modificar(usu.Foto);
                    cmd.Parameters.AddWithValue("@fot", usu.Foto.Id);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@fot", DBNull.Value);
                }

                return cmd.ExecuteNonQuery() > 0;
            }
        }
    }
}
