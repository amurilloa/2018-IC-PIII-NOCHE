﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoProyecto
{
    public partial class Form1 : Form
    {
        private PartidaBO pbo;
        private Partida partida;
        public Form1()
        {
            InitializeComponent();
            pbo = new PartidaBO();//TODO: en el Load
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            Refrescar();
            timer1.Start();
        }

        private void Refrescar()
        {
            partida = new Partida { Id = Int32.Parse(textBox1.Text.Trim()) };
            partida = pbo.Login(partida);
            if (partida.CanJug > 0)
            {
                pnlJuego.Enabled = true;
            }
            button2.Enabled = (partida.JugActual == Int32.Parse(textBox2.Text.Trim()));
            label1.Text = "Jugando... " + partida.JugActual;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pbo.Siguiente(partida, Int32.Parse(textBox2.Text.Trim()));
        }
    }
}
