﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoProyecto
{
    class Partida
    {
        public int Id { get; set; }
        public int CanJug { get; set; }
        public int JugActual { get; set; }
        public bool Estado { get; set; }
    }
}
