﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoProyecto
{
    class PartidaBO
    {
        internal Partida Login(Partida partida)
        {
            return new PartidaDAL().Verificar(partida);
        }

        internal void Siguiente(Partida partida, int jugador)
        {
            if (partida.CanJug == jugador)
            {
                new PartidaDAL().Actualizar(1, partida.Id);
            }
            else
            {
                new PartidaDAL().Actualizar(jugador + 1, partida.Id);
            }
        }
    }
}
