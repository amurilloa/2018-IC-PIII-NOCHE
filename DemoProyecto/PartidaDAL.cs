﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoProyecto
{
    class PartidaDAL
    {
        public Partida Verificar(Partida par)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"select id, num_jug, jug_act, estado from partida where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usuario", par.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarPartida(reader);
                }
            }
            return null;
        }

        internal void Actualizar(int actual, int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                Console.WriteLine(actual);
                Console.WriteLine(id);
                string sql = @"update partida  set jug_act = @actual  where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@actual", actual);
                cmd.ExecuteNonQuery();
            }
        }

        private Partida CargarPartida(NpgsqlDataReader reader)
        {
            Partida p = new Partida
            {
                Id = reader.GetInt32(0),
                CanJug = reader.GetInt32(1),
                JugActual = reader.GetInt32(2),
                Estado = reader.GetBoolean(3)
            };
            return p;
        }
    }
}
