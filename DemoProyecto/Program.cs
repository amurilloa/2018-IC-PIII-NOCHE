﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoProyecto
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 f1 = new Form1();
            f1.Show();
            Form1 f2 = new Form1();
            f2.Show();
            Application.Run(new Form1());
        }
    }
}


/*
 * 
CREATE TABLE public.partida
(
    id serial NOT NULL,
    num_jug integer,
    jug_act integer,
    estado boolean,
    CONSTRAINT partida_pkey PRIMARY KEY (id)
)
 
 * /
