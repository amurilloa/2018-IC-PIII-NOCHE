﻿namespace DemoWebService
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDolares = new System.Windows.Forms.TextBox();
            this.txtColones = new System.Windows.Forms.TextBox();
            this.lblTC = new System.Windows.Forms.Label();
            this.lblTV = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(566, 338);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 108);
            this.label2.TabIndex = 11;
            this.label2.Text = "USD";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(561, 184);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 108);
            this.label1.TabIndex = 10;
            this.label1.Text = "CRC";
            // 
            // txtDolares
            // 
            this.txtDolares.Font = new System.Drawing.Font("Arial Narrow", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDolares.Location = new System.Drawing.Point(12, 334);
            this.txtDolares.Name = "txtDolares";
            this.txtDolares.Size = new System.Drawing.Size(514, 118);
            this.txtDolares.TabIndex = 9;
            this.txtDolares.Text = "1.00";
            this.txtDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDolares.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConvertirACRC);
            // 
            // txtColones
            // 
            this.txtColones.Font = new System.Drawing.Font("Arial Narrow", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtColones.Location = new System.Drawing.Point(12, 180);
            this.txtColones.Name = "txtColones";
            this.txtColones.Size = new System.Drawing.Size(514, 118);
            this.txtColones.TabIndex = 8;
            this.txtColones.Text = "568.36";
            this.txtColones.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtColones.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConvertirAUSD);
            // 
            // lblTC
            // 
            this.lblTC.AutoSize = true;
            this.lblTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTC.Location = new System.Drawing.Point(517, 80);
            this.lblTC.Name = "lblTC";
            this.lblTC.Size = new System.Drawing.Size(299, 42);
            this.lblTC.TabIndex = 7;
            this.lblTC.Text = "Compra: 500.34";
            // 
            // lblTV
            // 
            this.lblTV.AutoSize = true;
            this.lblTV.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTV.Location = new System.Drawing.Point(552, 21);
            this.lblTV.Name = "lblTV";
            this.lblTV.Size = new System.Drawing.Size(264, 42);
            this.lblTV.TabIndex = 6;
            this.lblTV.Text = "Venta: 500.32";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 484);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDolares);
            this.Controls.Add(this.txtColones);
            this.Controls.Add(this.lblTC);
            this.Controls.Add(this.lblTV);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDolares;
        private System.Windows.Forms.TextBox txtColones;
        private System.Windows.Forms.Label lblTC;
        private System.Windows.Forms.Label lblTV;
    }
}

