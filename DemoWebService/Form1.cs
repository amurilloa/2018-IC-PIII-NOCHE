﻿using DemoWebService.BCCR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DemoWebService
{
    public partial class Form1 : Form
    {
        private double tipoCompra;
        private double tipoVenta;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //CON XMLPURO --> 
            //wsIndicadoresEconomicosSoapClient ws =
            //    new wsIndicadoresEconomicosSoapClient("wsIndicadoresEconomicosSoap");
            //string xml =
            //    ws.ObtenerIndicadoresEconomicosXML("318", DateTime.Now.ToString("dd/MM/yyyy"),
            //    DateTime.Now.ToString("dd/MM/yyyy"),
            //    "UTN", "N");
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);
            //XmlNodeList list = doc.GetElementsByTagName("NUM_VALOR");
            //tipoCompra = Convert.ToDouble(list[0].InnerText);
            //xml =
            //    ws.ObtenerIndicadoresEconomicosXML("317", DateTime.Now.ToString("dd/MM/yyyy"),
            //    DateTime.Now.ToString("dd/MM/yyyy"),
            //    "UTN", "N");
            //doc = new XmlDocument();
            //doc.LoadXml(xml);

            //list = doc.GetElementsByTagName("NUM_VALOR");
            //tipoVenta = Convert.ToDouble(list[0].InnerText);
            //lblTC.Text = String.Format("Compra: {0:F2}", tipoCompra);
            //lblTV.Text = String.Format("Venta: {0:F2}", tipoVenta);


            //CON PARSE --> 
            wsIndicadoresEconomicosSoapClient ws =
                new wsIndicadoresEconomicosSoapClient("wsIndicadoresEconomicosSoap");
            string xml =
                ws.ObtenerIndicadoresEconomicosXML("318", DateTime.Now.ToString("dd/MM/yyyy"),
                DateTime.Now.ToString("dd/MM/yyyy"),
                "UTN", "N");
            Datos_de_INGC011_CAT_INDICADORECONOMIC res = xml.ParseXML<Datos_de_INGC011_CAT_INDICADORECONOMIC>();
            tipoCompra = res.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR;

            xml =
                ws.ObtenerIndicadoresEconomicosXML("317", DateTime.Now.ToString("dd/MM/yyyy"),
                DateTime.Now.ToString("dd/MM/yyyy"),
                "UTN", "N");
            res = xml.ParseXML<Datos_de_INGC011_CAT_INDICADORECONOMIC>();
            tipoVenta = res.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR;

            lblTC.Text = String.Format("Compra: {0:F2}", tipoCompra);
            lblTV.Text = String.Format("Venta: {0:F2}", tipoVenta);


        }

        private void ConvertirAUSD(object sender, KeyEventArgs e)
        {
            if (Double.TryParse(txtColones.Text.Trim(), out double cant))
            {
                txtDolares.Text = String.Format("{0:F2}", cant / tipoCompra);
            }
            else if (String.IsNullOrEmpty(txtColones.Text))
            {
                txtDolares.Text = "";
            }
        }

        private void ConvertirACRC(object sender, KeyEventArgs e)
        {
            if (Double.TryParse(txtDolares.Text.Trim(), out double cant))
            {
                txtColones.Text = String.Format("{0:F2}", cant * tipoCompra);
            }
            else if (String.IsNullOrEmpty(txtDolares.Text))
            {
                txtColones.Text = "";
            }
        }
    }
}
