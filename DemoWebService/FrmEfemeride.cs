﻿using DemoWebService.IMN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoWebService
{
    public partial class FrmEfemeride : Form
    {
        public FrmEfemeride()
        {
            InitializeComponent();
        }

        private void FrmEfemeride_Load(object sender, EventArgs e)
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient("WSMeteorologico");
            string res = ws.pronosticoRegional(new pronosticoRegion());
            PRONOSTICO_REGIONAL pro = res.ParseXML<PRONOSTICO_REGIONAL>();
            foreach (PRONOSTICO_REGIONALREGION item in pro.REGIONES)
            {
                Console.WriteLine(item.nombre);
                Console.WriteLine(item.COMENTARIOMANANA);
                Console.WriteLine(item.ESTADOMANANA.imgPath);
            }
            Console.WriteLine(res);
        
        }
    }
}
