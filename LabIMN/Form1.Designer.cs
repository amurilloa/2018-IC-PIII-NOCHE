﻿namespace LabIMN
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlMR = new System.Windows.Forms.Panel();
            this.lblCMR = new System.Windows.Forms.Label();
            this.lblTMR = new System.Windows.Forms.Label();
            this.pbxMR = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlTR = new System.Windows.Forms.Panel();
            this.lblCTR = new System.Windows.Forms.Label();
            this.lblTTR = new System.Windows.Forms.Label();
            this.pbxTR = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlNR = new System.Windows.Forms.Panel();
            this.lblCNR = new System.Windows.Forms.Label();
            this.lblTNR = new System.Windows.Forms.Label();
            this.pbxNR = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxRegion = new System.Windows.Forms.ComboBox();
            this.pRONOSTICOREGIONALREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxCiudad = new System.Windows.Forms.ComboBox();
            this.pRONOSTICOREGIONALREGIONCIUDADBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblFase = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblLP = new System.Windows.Forms.Label();
            this.lblLS = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblSP = new System.Windows.Forms.Label();
            this.lblSS = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCMC = new System.Windows.Forms.Label();
            this.lblTMC = new System.Windows.Forms.Label();
            this.pbxMC = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCTC = new System.Windows.Forms.Label();
            this.lblTTC = new System.Windows.Forms.Label();
            this.pbxTC = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCNC = new System.Windows.Forms.Label();
            this.lblTNC = new System.Windows.Forms.Label();
            this.pbxNC = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlMR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMR)).BeginInit();
            this.pnlTR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTR)).BeginInit();
            this.pnlNR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxNR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONCIUDADBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMC)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTC)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxNC)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label1.Location = new System.Drawing.Point(596, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 44);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fecha: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.Controls.Add(this.cbxRegion);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(46, 113);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(544, 724);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pronóstico Regional";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pnlMR);
            this.flowLayoutPanel1.Controls.Add(this.pnlTR);
            this.flowLayoutPanel1.Controls.Add(this.pnlNR);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(21, 136);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(497, 555);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // pnlMR
            // 
            this.pnlMR.Controls.Add(this.lblCMR);
            this.pnlMR.Controls.Add(this.lblTMR);
            this.pnlMR.Controls.Add(this.pbxMR);
            this.pnlMR.Controls.Add(this.label2);
            this.pnlMR.Location = new System.Drawing.Point(3, 3);
            this.pnlMR.Name = "pnlMR";
            this.pnlMR.Size = new System.Drawing.Size(494, 177);
            this.pnlMR.TabIndex = 0;
            // 
            // lblCMR
            // 
            this.lblCMR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCMR.Location = new System.Drawing.Point(185, 88);
            this.lblCMR.Name = "lblCMR";
            this.lblCMR.Size = new System.Drawing.Size(301, 76);
            this.lblCMR.TabIndex = 6;
            this.lblCMR.Text = "Comentario";
            // 
            // lblTMR
            // 
            this.lblTMR.AutoSize = true;
            this.lblTMR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTMR.Location = new System.Drawing.Point(184, 41);
            this.lblTMR.Name = "lblTMR";
            this.lblTMR.Size = new System.Drawing.Size(87, 33);
            this.lblTMR.TabIndex = 5;
            this.lblTMR.Text = "Título";
            // 
            // pbxMR
            // 
            this.pbxMR.Location = new System.Drawing.Point(10, 10);
            this.pbxMR.Name = "pbxMR";
            this.pbxMR.Size = new System.Drawing.Size(158, 158);
            this.pbxMR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMR.TabIndex = 4;
            this.pbxMR.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(393, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "Mañana";
            // 
            // pnlTR
            // 
            this.pnlTR.Controls.Add(this.lblCTR);
            this.pnlTR.Controls.Add(this.lblTTR);
            this.pnlTR.Controls.Add(this.pbxTR);
            this.pnlTR.Controls.Add(this.label3);
            this.pnlTR.Location = new System.Drawing.Point(3, 186);
            this.pnlTR.Name = "pnlTR";
            this.pnlTR.Size = new System.Drawing.Size(494, 177);
            this.pnlTR.TabIndex = 1;
            // 
            // lblCTR
            // 
            this.lblCTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCTR.Location = new System.Drawing.Point(185, 91);
            this.lblCTR.Name = "lblCTR";
            this.lblCTR.Size = new System.Drawing.Size(301, 76);
            this.lblCTR.TabIndex = 8;
            this.lblCTR.Text = "Comentario";
            // 
            // lblTTR
            // 
            this.lblTTR.AutoSize = true;
            this.lblTTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTTR.Location = new System.Drawing.Point(184, 44);
            this.lblTTR.Name = "lblTTR";
            this.lblTTR.Size = new System.Drawing.Size(87, 33);
            this.lblTTR.TabIndex = 7;
            this.lblTTR.Text = "Título";
            // 
            // pbxTR
            // 
            this.pbxTR.Location = new System.Drawing.Point(11, 9);
            this.pbxTR.Name = "pbxTR";
            this.pbxTR.Size = new System.Drawing.Size(158, 158);
            this.pbxTR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxTR.TabIndex = 5;
            this.pbxTR.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(413, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 29);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tarde";
            // 
            // pnlNR
            // 
            this.pnlNR.Controls.Add(this.lblCNR);
            this.pnlNR.Controls.Add(this.lblTNR);
            this.pnlNR.Controls.Add(this.pbxNR);
            this.pnlNR.Controls.Add(this.label4);
            this.pnlNR.Location = new System.Drawing.Point(3, 369);
            this.pnlNR.Name = "pnlNR";
            this.pnlNR.Size = new System.Drawing.Size(494, 177);
            this.pnlNR.TabIndex = 2;
            // 
            // lblCNR
            // 
            this.lblCNR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNR.Location = new System.Drawing.Point(185, 92);
            this.lblCNR.Name = "lblCNR";
            this.lblCNR.Size = new System.Drawing.Size(301, 76);
            this.lblCNR.TabIndex = 8;
            this.lblCNR.Text = "Comentario";
            // 
            // lblTNR
            // 
            this.lblTNR.AutoSize = true;
            this.lblTNR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTNR.Location = new System.Drawing.Point(184, 45);
            this.lblTNR.Name = "lblTNR";
            this.lblTNR.Size = new System.Drawing.Size(87, 33);
            this.lblTNR.TabIndex = 7;
            this.lblTNR.Text = "Título";
            // 
            // pbxNR
            // 
            this.pbxNR.Location = new System.Drawing.Point(11, 8);
            this.pbxNR.Name = "pbxNR";
            this.pbxNR.Size = new System.Drawing.Size(158, 158);
            this.pbxNR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxNR.TabIndex = 6;
            this.pbxNR.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(407, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 29);
            this.label4.TabIndex = 4;
            this.label4.Text = "Noche";
            // 
            // cbxRegion
            // 
            this.cbxRegion.DataSource = this.pRONOSTICOREGIONALREGIONBindingSource;
            this.cbxRegion.DisplayMember = "nombre";
            this.cbxRegion.FormattingEnabled = true;
            this.cbxRegion.Location = new System.Drawing.Point(21, 68);
            this.cbxRegion.Name = "cbxRegion";
            this.cbxRegion.Size = new System.Drawing.Size(497, 50);
            this.cbxRegion.TabIndex = 0;
            this.cbxRegion.ValueMember = "idRegion";
            this.cbxRegion.SelectedIndexChanged += new System.EventHandler(this.SeleccionarRegion);
            // 
            // pRONOSTICOREGIONALREGIONBindingSource
            // 
            this.pRONOSTICOREGIONALREGIONBindingSource.DataSource = typeof(LabIMN.PRONOSTICO_REGIONALREGION);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flowLayoutPanel2);
            this.groupBox2.Controls.Add(this.cbxCiudad);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(604, 114);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 724);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pronóstico por Ciudad";
            // 
            // cbxCiudad
            // 
            this.cbxCiudad.DataSource = this.pRONOSTICOREGIONALREGIONCIUDADBindingSource;
            this.cbxCiudad.DisplayMember = "nombre";
            this.cbxCiudad.FormattingEnabled = true;
            this.cbxCiudad.Location = new System.Drawing.Point(14, 67);
            this.cbxCiudad.Name = "cbxCiudad";
            this.cbxCiudad.Size = new System.Drawing.Size(497, 50);
            this.cbxCiudad.TabIndex = 1;
            this.cbxCiudad.ValueMember = "id";
            this.cbxCiudad.SelectedIndexChanged += new System.EventHandler(this.SeleccionarCiudad);
            // 
            // pRONOSTICOREGIONALREGIONCIUDADBindingSource
            // 
            this.pRONOSTICOREGIONALREGIONCIUDADBindingSource.DataSource = typeof(LabIMN.PRONOSTICO_REGIONALREGIONCIUDAD);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(46, 843);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1105, 292);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Efemérides";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblFase);
            this.groupBox6.Location = new System.Drawing.Point(686, 79);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(413, 192);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Fase Lunar";
            // 
            // lblFase
            // 
            this.lblFase.AutoSize = true;
            this.lblFase.Location = new System.Drawing.Point(20, 58);
            this.lblFase.Name = "lblFase";
            this.lblFase.Size = new System.Drawing.Size(103, 42);
            this.lblFase.TabIndex = 2;
            this.lblFase.Text = "Sale:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblLP);
            this.groupBox5.Controls.Add(this.lblLS);
            this.groupBox5.Location = new System.Drawing.Point(359, 79);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(311, 192);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Luna";
            // 
            // lblLP
            // 
            this.lblLP.AutoSize = true;
            this.lblLP.Location = new System.Drawing.Point(14, 113);
            this.lblLP.Name = "lblLP";
            this.lblLP.Size = new System.Drawing.Size(145, 42);
            this.lblLP.TabIndex = 3;
            this.lblLP.Text = "Puesta:";
            // 
            // lblLS
            // 
            this.lblLS.AutoSize = true;
            this.lblLS.Location = new System.Drawing.Point(56, 58);
            this.lblLS.Name = "lblLS";
            this.lblLS.Size = new System.Drawing.Size(103, 42);
            this.lblLS.TabIndex = 2;
            this.lblLS.Text = "Sale:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblSP);
            this.groupBox4.Controls.Add(this.lblSS);
            this.groupBox4.Location = new System.Drawing.Point(21, 79);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(311, 192);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sol";
            // 
            // lblSP
            // 
            this.lblSP.AutoSize = true;
            this.lblSP.Location = new System.Drawing.Point(14, 113);
            this.lblSP.Name = "lblSP";
            this.lblSP.Size = new System.Drawing.Size(145, 42);
            this.lblSP.TabIndex = 3;
            this.lblSP.Text = "Puesta:";
            // 
            // lblSS
            // 
            this.lblSS.AutoSize = true;
            this.lblSS.Location = new System.Drawing.Point(56, 58);
            this.lblSS.Name = "lblSS";
            this.lblSS.Size = new System.Drawing.Size(103, 42);
            this.lblSS.TabIndex = 2;
            this.lblSS.Text = "Sale:";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.panel1);
            this.flowLayoutPanel2.Controls.Add(this.panel2);
            this.flowLayoutPanel2.Controls.Add(this.panel3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(13, 134);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(497, 555);
            this.flowLayoutPanel2.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblCMC);
            this.panel1.Controls.Add(this.lblTMC);
            this.panel1.Controls.Add(this.pbxMC);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(494, 177);
            this.panel1.TabIndex = 0;
            // 
            // lblCMC
            // 
            this.lblCMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCMC.Location = new System.Drawing.Point(185, 88);
            this.lblCMC.Name = "lblCMC";
            this.lblCMC.Size = new System.Drawing.Size(301, 76);
            this.lblCMC.TabIndex = 6;
            this.lblCMC.Text = "Comentario";
            // 
            // lblTMC
            // 
            this.lblTMC.AutoSize = true;
            this.lblTMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTMC.Location = new System.Drawing.Point(184, 41);
            this.lblTMC.Name = "lblTMC";
            this.lblTMC.Size = new System.Drawing.Size(87, 33);
            this.lblTMC.TabIndex = 5;
            this.lblTMC.Text = "Título";
            // 
            // pbxMC
            // 
            this.pbxMC.Location = new System.Drawing.Point(10, 10);
            this.pbxMC.Name = "pbxMC";
            this.pbxMC.Size = new System.Drawing.Size(158, 158);
            this.pbxMC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMC.TabIndex = 4;
            this.pbxMC.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(393, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 29);
            this.label7.TabIndex = 3;
            this.label7.Text = "Mañana";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblCTC);
            this.panel2.Controls.Add(this.lblTTC);
            this.panel2.Controls.Add(this.pbxTC);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(3, 186);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(494, 177);
            this.panel2.TabIndex = 1;
            // 
            // lblCTC
            // 
            this.lblCTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCTC.Location = new System.Drawing.Point(185, 91);
            this.lblCTC.Name = "lblCTC";
            this.lblCTC.Size = new System.Drawing.Size(301, 76);
            this.lblCTC.TabIndex = 8;
            this.lblCTC.Text = "Comentario";
            // 
            // lblTTC
            // 
            this.lblTTC.AutoSize = true;
            this.lblTTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTTC.Location = new System.Drawing.Point(184, 44);
            this.lblTTC.Name = "lblTTC";
            this.lblTTC.Size = new System.Drawing.Size(87, 33);
            this.lblTTC.TabIndex = 7;
            this.lblTTC.Text = "Título";
            // 
            // pbxTC
            // 
            this.pbxTC.Location = new System.Drawing.Point(11, 9);
            this.pbxTC.Name = "pbxTC";
            this.pbxTC.Size = new System.Drawing.Size(158, 158);
            this.pbxTC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxTC.TabIndex = 5;
            this.pbxTC.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(413, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 29);
            this.label10.TabIndex = 4;
            this.label10.Text = "Tarde";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblCNC);
            this.panel3.Controls.Add(this.lblTNC);
            this.panel3.Controls.Add(this.pbxNC);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Location = new System.Drawing.Point(3, 369);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(494, 177);
            this.panel3.TabIndex = 2;
            // 
            // lblCNC
            // 
            this.lblCNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNC.Location = new System.Drawing.Point(185, 92);
            this.lblCNC.Name = "lblCNC";
            this.lblCNC.Size = new System.Drawing.Size(301, 76);
            this.lblCNC.TabIndex = 8;
            this.lblCNC.Text = "Comentario";
            // 
            // lblTNC
            // 
            this.lblTNC.AutoSize = true;
            this.lblTNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTNC.Location = new System.Drawing.Point(184, 45);
            this.lblTNC.Name = "lblTNC";
            this.lblTNC.Size = new System.Drawing.Size(87, 33);
            this.lblTNC.TabIndex = 7;
            this.lblTNC.Text = "Título";
            // 
            // pbxNC
            // 
            this.pbxNC.Location = new System.Drawing.Point(11, 8);
            this.pbxNC.Name = "pbxNC";
            this.pbxNC.Size = new System.Drawing.Size(158, 158);
            this.pbxNC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxNC.TabIndex = 6;
            this.pbxNC.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(407, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 29);
            this.label13.TabIndex = 4;
            this.label13.Text = "Noche";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 1155);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Meteorológico";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnlMR.ResumeLayout(false);
            this.pnlMR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMR)).EndInit();
            this.pnlTR.ResumeLayout(false);
            this.pnlTR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTR)).EndInit();
            this.pnlNR.ResumeLayout(false);
            this.pnlNR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxNR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONCIUDADBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMC)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTC)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxNC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblFase;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblLP;
        private System.Windows.Forms.Label lblLS;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblSP;
        private System.Windows.Forms.Label lblSS;
        private System.Windows.Forms.ComboBox cbxRegion;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALREGIONBindingSource;
        private System.Windows.Forms.ComboBox cbxCiudad;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALREGIONCIUDADBindingSource;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlMR;
        private System.Windows.Forms.Panel pnlTR;
        private System.Windows.Forms.Panel pnlNR;
        private System.Windows.Forms.PictureBox pbxMR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbxTR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pbxNR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCMR;
        private System.Windows.Forms.Label lblTMR;
        private System.Windows.Forms.Label lblCTR;
        private System.Windows.Forms.Label lblTTR;
        private System.Windows.Forms.Label lblCNR;
        private System.Windows.Forms.Label lblTNR;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCMC;
        private System.Windows.Forms.Label lblTMC;
        private System.Windows.Forms.PictureBox pbxMC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblCTC;
        private System.Windows.Forms.Label lblTTC;
        private System.Windows.Forms.PictureBox pbxTC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblCNC;
        private System.Windows.Forms.Label lblTNC;
        private System.Windows.Forms.PictureBox pbxNC;
        private System.Windows.Forms.Label label13;
    }
}

