﻿using LabIMN.IMNService;
using System;
using System.Windows.Forms;

namespace LabIMN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient("WSMeteorologico");
            label1.Text = ws.fecha("").ToClean();

            CargarEfemerides(ws);
            CargarPronosticoRegional(ws);
        }

        private void CargarPronosticoRegional(WSMeteorologicoClient ws)
        {
            PRONOSTICO_REGIONAL reg = ws.pronosticoRegional(new pronosticoRegion()).ParseXML<PRONOSTICO_REGIONAL>();
            cbxRegion.DataSource = reg.REGIONES;
        }

        private void CargarEfemerides(WSMeteorologicoClient ws)
        {
            EFEMERIDES ef = ws.efemerides(new efemerides()).ParseXML<EFEMERIDES>();
            lblSS.Text = String.Format("Sale: {0}", ef.EFEMERIDE_SOL.SALE);
            lblSP.Text = String.Format("Puesta: {0}", ef.EFEMERIDE_SOL.SEPONE);

            lblLS.Text = String.Format("Sale: {0}", ef.EFEMERIDE_LUNA.SALE);
            lblLP.Text = String.Format("Puesta: {0}", ef.EFEMERIDE_LUNA.SEPONE);

            lblFase.Text = ef.FASELUNAR.Value;
        }

        private void SeleccionarRegion(object sender, EventArgs e)
        {
            PRONOSTICO_REGIONALREGION reg = cbxRegion.SelectedItem as PRONOSTICO_REGIONALREGION;
            //TODO: Crear una constante para la base de la ruta de las imagenes 
            pbxMR.ImageLocation = "https://www.imn.ac.cr"+reg.ESTADOMANANA.imgPath;
            lblTMR.Text = reg.ESTADOMANANA.Value;
            lblCMR.Text = reg.COMENTARIOMANANA;

            pbxTR.ImageLocation = "https://www.imn.ac.cr" + reg.ESTADOTARDE.imgPath;
            lblTTR.Text = reg.ESTADOTARDE.Value;
            lblCTR.Text = reg.COMENTARIOTARDE;

            pbxNR.ImageLocation = "https://www.imn.ac.cr" + reg.ESTADONOCHE.imgPath;
            lblTNR.Text = reg.ESTADONOCHE.Value;
            lblCNR.Text = reg.COMENTARIONOCHE;

            CargarCiudades(reg.CIUDADES);
        }

        private void CargarCiudades(PRONOSTICO_REGIONALREGIONCIUDAD[] ciudades)
        {
            cbxCiudad.DataSource = ciudades;
        }

        private void SeleccionarCiudad(object sender, EventArgs e)
        {
            PRONOSTICO_REGIONALREGIONCIUDAD ciu = cbxCiudad.SelectedItem as PRONOSTICO_REGIONALREGIONCIUDAD;
            pbxMC.ImageLocation = "https://www.imn.ac.cr" + ciu.ESTADOMANANA.imgPath;
            lblTMC.Text = ciu.ESTADOMANANA.Value;
            lblCMC.Text = ciu.COMENTARIOMANANA;

            pbxTC.ImageLocation = "https://www.imn.ac.cr" + ciu.ESTADOTARDE.imgPath;
            lblTTC.Text = ciu.ESTADOTARDE.Value;
            lblCTC.Text = ciu.COMENTARIOTARDE;

            pbxNC.ImageLocation = "https://www.imn.ac.cr" + ciu.ESTADONOCHE.imgPath;
            lblTNC.Text = ciu.ESTADONOCHE.Value;
            lblCNC.Text = ciu.COMENTARIONOCHE;
        }
    }
}