﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Fecha
    {
        public int Dia { get; private set; }
        public int Mes { get; private set; }
        public int Anno { get; private set; }

        public Fecha()
        {
            Dia = 1;
            Mes = 1;
            Anno = 1900;
        }

        public Fecha(int dia, int mes, int anno)
        {
            ModificarFecha(dia, mes, anno);
        }

        // Verifica que la fecha sea valida
        private bool Verficar(int dia, int mes, int anno)
        {
            if (dia < 1 || dia > 31)
            {
                return false;
            }

            if (mes < 1 || mes > 12)
            {
                return false;
            }
            if (anno < 1) //Ni idea si existe
            {
                return false;
            }

            if (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11))
            {
                return false;
            }

            //Febrero
            if (dia >= 29 && mes == 2 && !Bisiesto(anno))
            {
                return false;
            }
            if (dia > 29 && mes == 2 && Bisiesto(anno))
            {
                return false;
            }
            return true;
        }

        private bool Bisiesto(int anno)
        {
            if (anno % 4 == 0)
            {
                if (anno % 100 == 0)
                {
                    return anno % 400 == 0;
                }
                return true;
            }
            return false;
        }

        internal void ModificarFecha(int dia, int mes, int anno)
        {
            if (Verficar(dia, mes, anno))
            {
                Dia = dia;
                Mes = mes;
                Anno = anno;
            }
            else
            {
                Dia = 1;
                Mes = 1;
                Anno = 1900;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal string MostrarFecha()
        {
            return String.Format("{0:00}/{1:00}/{2:0000}", Dia, Mes, Anno);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal string MostrarFechaL()
        {
            EMes mes = (EMes)Mes;
            return String.Format("{0} de {1} de {2:0000}", Dia, mes, Anno);

        }

    }
}