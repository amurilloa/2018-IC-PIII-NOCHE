﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Gasolinera
    {
        public const double GAL_LIT = 3.7854;

        public double Precio { get; set; }

        public Gasolinera()
        {
            Precio = 720;//xLitro
        }

        internal double Total(double galones)
        {
            return galones * GAL_LIT * Precio;
        }
    }
}
