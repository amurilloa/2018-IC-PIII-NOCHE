﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Mesa
    {
        public Producto[] Menu { get; private set; }
        public int[] Cantidades { get; set; }

        public Mesa()
        {
            Menu = new Producto[]{
                new Producto { Descripcion = "Hamburguesa Sencilla", Precio = 15},
                new Producto { Descripcion = "Hamburguesa C Queso", Precio = 18 },
                new Producto { Descripcion = "Hamburguesa Especial", Precio = 20},
                new Producto { Descripcion = "Papas Fritas", Precio = 8},
                new Producto { Descripcion = "Refresco", Precio = 5},
                new Producto { Descripcion = "Postre", Precio = 6}
                };
            Cantidades = new int[Menu.Length];
        }

        internal void CapturarOrden(int art, int can)
        {
            Cantidades[art - 1] += can;
        }

        internal double CalcularTotal()
        {
            double total = 0;
            for (int i = 0; i < Cantidades.Length; i++)
            {
                total += Cantidades[i] * Menu[i].Precio;
            }
            return total;
        }

        internal string MostrarMenu()
        {
            string menu = "";
            int i = 1;
            foreach (var item in Menu)
            {
                menu += i +". "+ item.Descripcion + (item.Descripcion.Length >= 13 ? "\t" : "\t\t") + "($" + item.Precio + ")\n";
                i++;
            }
            menu += "7. Total";
            return menu + "\nSeleccione una opción: ";
        }
    }
}
