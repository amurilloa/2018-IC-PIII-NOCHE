﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Program
    {
        static void Main(string[] args)
        {

            //Circunferencia moneda = new Circunferencia
            //{
            //    Radio = 1.4
            //};
            //Circunferencia rueda = new Circunferencia { Radio = 10.2, Nombre = "Rueda" };

            //moneda.Radio = 10.2;
            //Console.WriteLine(moneda.Radio);

            //Console.WriteLine(rueda.CalcularArea());
            //Console.WriteLine(rueda.CalcularPerimetro());
            //Console.WriteLine(moneda.CalcularArea());
            //Console.WriteLine(moneda.CalcularPerimetro());

            //Rectangulo pared = new Rectangulo { Ancho = 7, Largo = 3 };
            //Rectangulo ventana = new Rectangulo { Ancho = 2, Largo = 2 };
            //double areaP = pared.CalcularArea();
            //double areaV = ventana.CalcularArea();
            //double aPintar = areaP - areaV;
            //Console.WriteLine("Área de Pared: {0: 0.00}m²", areaP);
            //Console.WriteLine("Área de Ventana: {0: 0.00}m²", areaV);
            //Console.WriteLine("Área a Pintar: {0: 0.00}m²", aPintar);
            //int m = (int)aPintar * 10;
            //int h = m / 60;
            //m %= 60;
            //Console.WriteLine("El pintor tarda {0}hrs{1}min", h, m);

            //Fecha = DateTime

            //Fecha fec = new Fecha(29, 2, 4);
            //Fecha fec2 = new Fecha(29,2,2001);
            //Console.WriteLine(fec.MostrarFecha());
            //fec2.ModificarFecha(29, 2, 2016);
            //Console.WriteLine(fec2.MostrarFecha());
            //Console.WriteLine(fec2.MostrarFechaL());

            //Articulo art = new Articulo(1, "Arroz") { Precio = 1890 };
            //art.Cantidad = 5;

            //Temperatura temp = new Temperatura { Celsius = 30 };
            //Console.WriteLine(temp.ConvertirAFarenheit());

            //CambioDivisas cd = new CambioDivisas(571) { Colones = 100000 };
            //Console.WriteLine("{0:0.00}",cd.ConvertirADolares());

            //Gasolinera gas = new Gasolinera();
            //double galones = 7;
            //Console.WriteLine("Por {0} gal debe pagar {1:0.00} CRC", galones, gas.Total(galones)); 


            Mesa[] mesas = new Mesa[]
            {
                new Mesa(), new Mesa(), new Mesa(), new Mesa(), new Mesa()
            };
            Console.WriteLine("Seleccione el número de mesa (1-5): ");
            int op = Int32.Parse(Console.ReadLine());
            Mesa m = mesas[op];

            while (true)
            {
                Console.Clear();
                Console.Write(m.MostrarMenu());
                op = Int32.Parse(Console.ReadLine());
                if (op == 7)
                {
                    Console.WriteLine("Total a pagar: ${0:0.00}", m.CalcularTotal());
                    break;
                }
                else
                {
                    Console.Write("Digite la cantidad: ");
                    int can = Int32.Parse(Console.ReadLine());
                    m.CapturarOrden(op, can);
                }
            }
            Console.ReadKey();

        }
    }
}
