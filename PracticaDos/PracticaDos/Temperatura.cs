﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Temperatura
    {
        public int Celsius { get; set; }

        internal double ConvertirAFarenheit()
        {
            return Celsius * 9 / 5 + 32;
        }
    }
}
