﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioDos
{
    class Logica
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        /// <param name="n3"></param>
        /// <returns></returns>
        internal string Operacion(int n1, int n2, int n3)
        {
            if (n1 > 0)
            {
                return n2 + "*" + n3 + "=" + (n2 * n3); //"{0}*{1}={2}"
            }
            return n2 + "+" + n3 + "=" + (n2 + n3); //"{0}+{1}={2}"
        }

        internal int Invertir(int numero)
        {
            int inv = 0;
            while (numero != 0)
            {
                int ult = numero % 10;
                inv = inv * 10 + ult;
                numero /= 10;
            }
            return inv;
        }

        internal int Media(int[] arreglo)
        {
            int sum = 0;
            foreach (var item in arreglo)
            {
                sum += item;
            }
            return sum / arreglo.Length;
        }

        internal int Mayor(int[] arreglo)
        {
            //arreglo = { -1,-2,-3,-6};
            int mayor = arreglo[0];
            foreach (var item in arreglo)
            {
                if (item > mayor)
                {
                    mayor = item;
                }
            }
            return mayor;
        }

        internal int Menor(int[] arreglo)
        {
            int menor = arreglo[0];
            foreach (var item in arreglo)
            {
                if (item < menor)
                {
                    menor = item;
                }
            }
            return menor;
        }
    }
}
