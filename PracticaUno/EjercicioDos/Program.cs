﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioDos
{
    class Program
    {
        static int LeerInt(string texto)
        {
            int n1 = 0;
            do
            {
                Console.Write(texto + ": ");
            } while (!Int32.TryParse(Console.ReadLine(), out n1));
            return n1;
        }
        static void Main(string[] args)
        {
            Logica log = new Logica();

            //Console.WriteLine("Ejercicio 1");
            //Console.Write("Digite su nombre: ");
            //string nombre = Console.ReadLine();
            //Console.WriteLine("Hola {0}, hoy es {1}", nombre, DateTime.Now);
            //Console.ReadKey();

            //while (true)
            //{
            //    Console.Clear();
            //    Console.WriteLine("Ejercicio 2");

            //    int n1 = LeerInt("Digite el número 1");
            //    int n2 = LeerInt("Digite el número 2");
            //    int n3 = LeerInt("Digite el número 3");
            //    Console.WriteLine(log.Operacion(n1, n2, n3));
            //    Console.WriteLine("s: salir, otra para repetir");
            //    char res = Console.ReadKey().KeyChar;
            //    if (res == 's')
            //    {
            //        break;
            //    }
            //}
            //Console.Clear();
            //Console.WriteLine("Ejercicio 4");
            //int numero = LeerInt("Digite un número positivo");
            //Console.WriteLine(log.Invertir(numero));

            int n = LeerInt("Cantidad de Números");
            int[] arreglo = new int[n];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = LeerInt("Digite el #" + (i + 1));
            }
            Console.WriteLine("Menor: {0}", log.Menor(arreglo));
            Console.WriteLine("Mayor: {0}", log.Mayor(arreglo));
            Console.WriteLine("Media: {0}", log.Media(arreglo));


            Console.ReadKey();


        }
    }
}
