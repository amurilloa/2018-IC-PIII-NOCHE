﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioTres
{
    class Logica
    {
        internal int CantConejos(int meses)
        {
            int canHijos = 0;
            int canPadres = 1;
            int canMeses = 0;
            while (canMeses < meses)
            {
                int temp = canPadres;
                canPadres = canPadres + canHijos;
                canHijos = temp;
                canMeses++;
            }
            return (canPadres + canHijos) * 2;
        }

        internal int CantMeses(int conejos)
        {
            int mes = 0;
            while (true)
            {
                int can = CantConejos(mes);
                if (can >= conejos)
                {
                    return mes;
                }
                mes++;
            }
        }

        internal int CantMesesDos(int conejos)
        {
            int meses = -1;
            int canConejos = 0;
            int canPadres = 1;
            int canHijos = 0;

            while (canConejos <= conejos)
            {
                int temp = canPadres;
                canPadres = canPadres + canHijos;
                canHijos = temp;
                canConejos = (canHijos + canPadres) * 2;
                meses++;
            }
            return meses;
        }

        internal bool EsPrimo(int numero)
        {
            int canDiv = 0;
            for (int i = 1; i <= numero; i++)
            {
                if (numero % i == 0)
                {
                    canDiv++;
                }
                if (canDiv > 2)
                {
                    return false;
                }
            }
            return canDiv == 2;
        }

        internal double DiezMilPrimos()
        {
            DateTime fi = DateTime.Now;

            int can = 0;
            int con = 0;
            while (can < 10000)
            {
                if (EsPrimo(con))
                {
                    can++;
                }
                con++;
            }
            DateTime ff = DateTime.Now;
            Console.WriteLine(con);
            Console.WriteLine(can);

            return ff.Subtract(fi).TotalSeconds;
        }

    }
}
