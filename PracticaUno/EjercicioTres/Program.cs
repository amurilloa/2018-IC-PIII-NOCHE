﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioTres
{
    class Program
    {
        static void Main(string[] args)
        {
            Logica log = new Logica();
            int meses = 8;
            Console.WriteLine("En {0} meses tendrá {1} conejos", meses, log.CantConejos(meses));
            int conejos = 110;
            Console.WriteLine("Tendrá {0} conejos, en {1} meses", conejos, log.CantMeses(conejos));
            Console.WriteLine("Tendrá {0} conejos, en {1} meses", conejos, log.CantMesesDos(conejos));
            int numero = 6;
            Console.WriteLine("{0}: {1}", numero, log.EsPrimo(numero));
            numero = 104743;
            Console.WriteLine("{0}: {1}", numero, log.EsPrimo(numero));
            Console.WriteLine("Tiempo de los 10k primos {0:0.0}", log.DiezMilPrimos());
            Console.ReadKey();
        }
    }
}
