﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Operaciones
    {
        /// <summary>
        /// Suma dos números enteros
        /// </summary>
        /// <param name="num1">Número Uno</param>
        /// <param name="num2">Número Dos</param>
        /// <returns>Suma de los dos números</returns>
        internal int Sumar(int num1, int num2)
        {
            return num1 + num2;
        }

        internal int Resta(int num1, int num2)
        {
            return num1 - num2;
        }

        internal int Multiplicar(int num1, int num2)
        {
            return num1 * num2;
        }

        internal int Dividir(int num1, int num2)
        {
            if (num2 == 0)
            {
                return 0; //throw...
            }
            return num1 / num2;
        }

        internal int[] LlenarArreglo(int num)
        {
            int[] arreglo = new int[num];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = num;
            }
            return arreglo;
        }

        internal string ImprimirArreglo(int[] arreglo)
        {
            string texto = "";
            if (arreglo != null)
            {
                foreach (var item in arreglo)
                {
                    texto += item + ", ";
                }
            }
            return texto.Substring(0, texto.Length - 2);
        }

        internal void LlenarArreglo(int num, int[] arreglo)
        {
            arreglo = new int[num];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = num;
            }
        }
    }
}
