﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Program
    {
        static void Main(string[] args)
        {
            string menu = "\nMenu Práctica Uno\n" +
                "1. Sumar\n" +
                "2. Restar\n" +
                "3. Multiplicar\n" +
                "4. Dividir\n" +
                "5. Llenar Arreglo\n" +
                "6. Imprimir\n" +
                "7. Salir\n" +
                "Seleccione una opción: ";

            bool salir = false;
            Operaciones ope = new Operaciones();
            int[] arreglo = null;
            while (!salir)
            {
                Console.Clear();
                Console.Write(menu);
                int op = Int32.Parse(Console.ReadLine());
                int num1 = 0;
                int num2 = 0;
                if (op > 0 && op < 5)
                {
                    Console.Write("Digite el número 1: ");
                    num1 = Int32.Parse(Console.ReadLine());
                    Console.Write("Digite el número 2: ");
                    num2 = Int32.Parse(Console.ReadLine());
                }
                switch (op)
                {
                    case 1:
                        int res = ope.Sumar(num1, num2);
                        Console.WriteLine("{0}+{1}={2}", num1, num2, res);
                        break;
                    case 2:
                        res = ope.Resta(num1, num2);
                        Console.WriteLine("{0}-{1}={2}", num1, num2, res);
                        break;
                    case 3:
                        res = ope.Multiplicar(num1, num2);
                        Console.WriteLine("{0}*{1}={2}", num1, num2, res);
                        break;
                    case 4:
                        res = ope.Dividir(num1, num2);
                        Console.WriteLine("{0}/{1}={2}", num1, num2, res);
                        break;
                    case 5:
                        Console.Write("Digite un número: ");
                        int num = Int32.Parse(Console.ReadLine());
                        arreglo = ope.LlenarArreglo(num);
                        break;
                    case 6:
                        Console.WriteLine(ope.ImprimirArreglo(arreglo));
                        break;
                    case 7:
                        Console.WriteLine("Presione cualquier tecla para salir");
                        salir = true;
                        break;
                    default:
                        Console.WriteLine("Opción Inválida");
                        break;
                }
                Console.ReadKey();
            }
        }
    }
}
