﻿namespace QuizUnoAllanMurillo
{
    partial class FrmQuizUno
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ejerciciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ternasPitagóricasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluarFunciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sumatoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rtxtResultados = new System.Windows.Forms.RichTextBox();
            this.txtParametroUno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.evaluarFuncionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejerciciosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(845, 42);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ejerciciosToolStripMenuItem
            // 
            this.ejerciciosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ternasPitagóricasToolStripMenuItem,
            this.evaluarFunciónToolStripMenuItem,
            this.sumatoriaToolStripMenuItem,
            this.evaluarFuncionToolStripMenuItem});
            this.ejerciciosToolStripMenuItem.Name = "ejerciciosToolStripMenuItem";
            this.ejerciciosToolStripMenuItem.Size = new System.Drawing.Size(124, 38);
            this.ejerciciosToolStripMenuItem.Text = "Ejercicios";
            // 
            // ternasPitagóricasToolStripMenuItem
            // 
            this.ternasPitagóricasToolStripMenuItem.Name = "ternasPitagóricasToolStripMenuItem";
            this.ternasPitagóricasToolStripMenuItem.Size = new System.Drawing.Size(303, 38);
            this.ternasPitagóricasToolStripMenuItem.Text = "Ternas Pitagóricas";
            this.ternasPitagóricasToolStripMenuItem.Click += new System.EventHandler(this.BuscarTernas);
            // 
            // evaluarFunciónToolStripMenuItem
            // 
            this.evaluarFunciónToolStripMenuItem.Name = "evaluarFunciónToolStripMenuItem";
            this.evaluarFunciónToolStripMenuItem.Size = new System.Drawing.Size(303, 38);
            this.evaluarFunciónToolStripMenuItem.Text = "Evaluar Ecuación";
            this.evaluarFunciónToolStripMenuItem.Click += new System.EventHandler(this.EvaluarEcuacion);
            // 
            // sumatoriaToolStripMenuItem
            // 
            this.sumatoriaToolStripMenuItem.Name = "sumatoriaToolStripMenuItem";
            this.sumatoriaToolStripMenuItem.Size = new System.Drawing.Size(303, 38);
            this.sumatoriaToolStripMenuItem.Text = "Sumatoria";
            this.sumatoriaToolStripMenuItem.Click += new System.EventHandler(this.Sumatoria);
            // 
            // rtxtResultados
            // 
            this.rtxtResultados.Location = new System.Drawing.Point(39, 120);
            this.rtxtResultados.Name = "rtxtResultados";
            this.rtxtResultados.Size = new System.Drawing.Size(730, 556);
            this.rtxtResultados.TabIndex = 3;
            this.rtxtResultados.Text = "";
            // 
            // txtParametroUno
            // 
            this.txtParametroUno.Location = new System.Drawing.Point(218, 67);
            this.txtParametroUno.Name = "txtParametroUno";
            this.txtParametroUno.Size = new System.Drawing.Size(119, 31);
            this.txtParametroUno.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Parámetro Uno:";
            // 
            // evaluarFuncionToolStripMenuItem
            // 
            this.evaluarFuncionToolStripMenuItem.Name = "evaluarFuncionToolStripMenuItem";
            this.evaluarFuncionToolStripMenuItem.Size = new System.Drawing.Size(303, 38);
            this.evaluarFuncionToolStripMenuItem.Text = "Evaluar Funcion";
            this.evaluarFuncionToolStripMenuItem.Click += new System.EventHandler(this.EvaluarFuncion);
            // 
            // FrmQuizUno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 737);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtParametroUno);
            this.Controls.Add(this.rtxtResultados);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmQuizUno";
            this.Text = "QuizUno";
            this.Load += new System.EventHandler(this.FrmQuizUno_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ejerciciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ternasPitagóricasToolStripMenuItem;
        private System.Windows.Forms.RichTextBox rtxtResultados;
        private System.Windows.Forms.ToolStripMenuItem evaluarFunciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sumatoriaToolStripMenuItem;
        private System.Windows.Forms.TextBox txtParametroUno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem evaluarFuncionToolStripMenuItem;
    }
}

