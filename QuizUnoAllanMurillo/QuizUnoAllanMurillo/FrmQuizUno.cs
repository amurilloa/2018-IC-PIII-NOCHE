﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizUnoAllanMurillo
{
    public partial class FrmQuizUno : Form
    {
        private Logica logica;

        public FrmQuizUno()
        {
            InitializeComponent();
        }

        private void FrmQuizUno_Load(object sender, EventArgs e)
        {
            logica = new Logica();
        }

        private void BuscarTernas(object sender, EventArgs e)
        {
            rtxtResultados.Text = logica.BuscarTernas();
        }

        private void EvaluarEcuacion(object sender, EventArgs e)
        {
            rtxtResultados.Text = logica.EvaluarEcuacion();
        }

        private void Sumatoria(object sender, EventArgs e)
        {
            double limSup;
            if (Double.TryParse(txtParametroUno.Text, out limSup))
            {
                rtxtResultados.Text = logica.Sumatoria(limSup);
            }
            else
            {
                rtxtResultados.Text = "Digite el límite superior en el parámetro uno";
            }

        }

        private void EvaluarFuncion(object sender, EventArgs e)
        {
            rtxtResultados.Text = logica.EvaluarFuncion();
        }
    }
}
