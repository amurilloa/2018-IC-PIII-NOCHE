﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizUnoAllanMurillo
{
    class Logica
    {
        internal string BuscarTernas()
        {
            string ternas = "";
            for (int a = 0; a < 25; a++)
            {
                for (int b = a + 1; b < 25; b++)
                {
                    for (int c = b + 1; c < 34; c++)
                    {
                        if (Math.Pow(a, 2) + Math.Pow(b, 2) == Math.Pow(c, 2))
                        {
                            ternas += String.Format("- {0},{1},{2}\n", a, b, c);
                        }
                    }
                }
            }
            return ternas;
        }

        internal string EvaluarEcuacion()
        {
            string resultados = "";

            double x = 20;
            while (x <= 30)
            {
                double y = Math.Pow(x, 2) - 4 * x + 6;
                resultados += String.Format("x:{0:F1} --> y:{1:F1}\n", x, y);
                x += 0.5;
            }
            return resultados;
        }


        internal string EvaluarFuncion()
        {
            string resultados = "";

            double x = 1;
            while (x <= 2)
            {
                double y = 4 * Math.Pow(x, 2) - 16 * x + 15;
                Console.WriteLine(y);
                string mensaje = y >= 0.0 ? "POSITIVO" : "NO POSITIVO";
                resultados += String.Format("x:{0:F1} --> y:{1} : {2}\n", x, y, mensaje);
                x += 0.1;
            }
            return resultados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="limSup"></param>
        /// <returns></returns>
        internal string Sumatoria(double limSup)
        {
            string res = "";
            double total = 0;
            double cant = 1.0;
            while (total < limSup)
            {
                total += 1 / cant;
                res += cant == 1 ? "1" : String.Format("+ 1/{0} ", cant);
                cant++;
            }

            return res + String.Format("={0:F2}", total)
                + String.Format("\nCantidad de elementos:{0}", (cant - 1));
        }
    }
}
