﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorAllanMurilloBOL;
using VisorAllanMurilloENT;

namespace VisorAllanMurillo
{
    public partial class Form1 : Form
    {
        private List<EImagen> fotos;
        private int numFoto;
        private ImagenBOL ibo;
        private int xP;
        private int yP;

        public Form1()
        {
            InitializeComponent();
        }

        private void PasarFoto(object sender, EventArgs e)
        {
            PasarFoto();
        }

        private void PasarFoto()
        {
            Console.WriteLine("Cambio de foto");
            Console.WriteLine(fotos.Count);
            if (fotos.Count > numFoto && numFoto >= 0)
            {
                pictureBox1.Image = fotos.ElementAt(numFoto).Imagen;
                numFoto++;
            }
            else
            {
                numFoto = 0;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ibo = new ImagenBOL();
            fotos = ibo.CargarImagenes("");
            numFoto = 0;
            xP = Location.X + 113;
            yP = Location.Y + 380;
        }

        private void btnPlayPause_Click(object sender, EventArgs e)
        {
            timer1.Enabled = !timer1.Enabled;
            btnPlayPause.Text = timer1.Enabled ? "||" : ">";
        }

        private void nuevaImagenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImagen img = new FrmImagen();
            img.ShowDialog();
            fotos = ibo.CargarImagenes("");
            numFoto = 0;
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ibo.Eliminar(fotos.ElementAt(numFoto - 1));
            fotos = ibo.CargarImagenes("");
            numFoto = 0;
        }

        private void modificarActualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImagen frm = new FrmImagen
            {
                Imagen = fotos.ElementAt(numFoto - 1)
            };
            frm.ShowDialog();
            fotos = ibo.CargarImagenes("");
            numFoto = 0;
        }

        private void btnSig_Click(object sender, EventArgs e)
        {
            PasarFoto();
        }

        private void btnAnt_Click(object sender, EventArgs e)
        {
            numFoto -= 2;
            PasarFoto();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            btnPlayPause.Text = ">";
        }


        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            int x = Cursor.Position.X;
            int y = Cursor.Position.Y;
            int limX = xP + 312;
            int limY = yP + 100;

            if (x > xP && x < limX && y > yP && y < limY)
            {
                if (panel1.Location.Y > (yP - Location.Y))
                {
                    panel1.Location = new Point(panel1.Location.X, panel1.Location.Y - 7);
                    Thread.Sleep(10);
                    Refresh();
                }
            }
            else
            {
                if (panel1.Location.Y < (yP - Location.Y + 100))
                {
                    panel1.Location = new Point(panel1.Location.X, panel1.Location.Y + 7);
                    Thread.Sleep(10);
                    Refresh();
                }
            }
        }
    }
}
