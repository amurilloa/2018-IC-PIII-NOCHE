﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorAllanMurilloBOL;
using VisorAllanMurilloENT;

namespace VisorAllanMurillo
{
    public partial class FrmImagen : Form
    {
        public EImagen Imagen { get; set; }

        public FrmImagen()
        {
            InitializeComponent();
        }

        private void FrmImagen_Load(object sender, EventArgs e)
        {
            if (Imagen != null)
            {
                //Cargar datos en la interfaz
                CargarDatos();
            }
        }

        private void CargarDatos()
        {
            txtTitulo.Text = Imagen.Titulo;
            txtTipo.Text = Imagen.Tipo;
            txtDesc.Text = Imagen.Descripcion;
            pictureBox1.Image = Imagen.Imagen;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EImagen imagen = new EImagen
            {
                Id = Imagen != null ? Imagen.Id : 0,
                Titulo = txtTitulo.Text.Trim(),
                Tipo = txtTipo.Text.Trim(),
                Descripcion = txtDesc.Text.Trim(),
                Imagen = pictureBox1.Image
            };
            ImagenBOL ibol = new ImagenBOL();
            imagen = ibol.Guardar(imagen);
            if (imagen.Id > 0)
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}
