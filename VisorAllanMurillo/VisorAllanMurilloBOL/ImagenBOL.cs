﻿using System;
using System.Collections.Generic;
using VisorAllanMurilloDAL;
using VisorAllanMurilloENT;

namespace VisorAllanMurilloBOL
{
    public class ImagenBOL
    {
        public List<EImagen> CargarImagenes(string filtro)
        {
            return new ImagenDAL().CargarTodo(filtro);
        }

        public EImagen Guardar(EImagen imagen)
        {
            //Validar los datos de la imangen 
            if (String.IsNullOrEmpty(imagen.Titulo))
            {
                throw new Exception("Título requerido");
            }

            if (String.IsNullOrEmpty(imagen.Tipo))
            {
                throw new Exception("Tipo requerido");
            }

            if (String.IsNullOrEmpty(imagen.Descripcion))
            {
                throw new Exception("Descripción requerida");
            }

            if (imagen.Imagen == null)
            {
                throw new Exception("Imagen requerida");
            }

            if (imagen.Id > 0)
            {
                return new ImagenDAL().Modificar(imagen);
            }
            else
            {
                return new ImagenDAL().Insertar(imagen);
            }
        }

        public void Eliminar(EImagen eImagen)
        {
            if (eImagen.Id > 0)
            {
                new ImagenDAL().Eliminar(eImagen);
            }
        }
    }
}