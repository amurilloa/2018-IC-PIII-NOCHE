﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using VisorAllanMurilloENT;

namespace VisorAllanMurilloDAL
{
    public class ImagenDAL
    {
        public List<EImagen> CargarTodo(string filtro) {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                List<EImagen> imagenes = new List<EImagen>();

                con.Open();
                string sql = @"select id,  titulo, tipo, descripcion, imagen
                               from imagen order by id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                if (!String.IsNullOrEmpty(filtro))
                {
                    sql = @"select  id,titulo, tipo, descripcion, imagen
                               from imagen where lower(tipo) like @filtro 
                               order by id";
                    cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@filtro", filtro.ToLower() + "%");

                }
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    imagenes.Add(CargarFoto(reader));
                }

                return imagenes;
            }
        }

        public EImagen Insertar(EImagen imagen)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"insert into imagen(titulo, tipo, descripcion, imagen) values (@tit, @tip, @des, @ima) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                imagen.Imagen.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@tit", imagen.Titulo);
                cmd.Parameters.AddWithValue("@tip",imagen.Tipo);
                cmd.Parameters.AddWithValue("@des", imagen.Descripcion);
                cmd.Parameters.AddWithValue("@ima", pic);

                imagen.Id = Convert.ToInt32(cmd.ExecuteScalar());

                return imagen;
            }
        }

        public EImagen FotoId(int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                EImagen foto = new EImagen();
                con.Open();
                string sql = @"select id, titulo, tipo, descripcion, imagen
                               from imagen where id = @id  order by id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarFoto(reader);
                }

                return foto;
            }
        }

        private EImagen CargarFoto(NpgsqlDataReader reader)
        {
            EImagen foto = new EImagen();
            foto.Id = Convert.ToInt32(reader["id"]);
            byte[] f = new byte[0];
            f = (byte[])reader["imagen"];
            MemoryStream stream = new MemoryStream(f);
            foto.Imagen = Image.FromStream(stream);
            foto.Titulo = reader["titulo"].ToString();
            foto.Tipo = reader["tipo"].ToString();
            foto.Descripcion= reader["descripcion"].ToString();
            return foto;
        }

        public EImagen Modificar(EImagen imagen)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"UPDATE imagen
	                            SET titulo = @tit, tipo= @tip, descripcion= @des,imagen = @ima
	                            WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                imagen.Imagen.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@ima", pic);
                cmd.Parameters.AddWithValue("@tit", imagen.Titulo);
                cmd.Parameters.AddWithValue("@tip", imagen.Tipo);
                cmd.Parameters.AddWithValue("@des", imagen.Descripcion);
                cmd.Parameters.AddWithValue("@id", imagen.Id);
                cmd.ExecuteNonQuery();
                return imagen;
            }
            
        }

        public void Eliminar(EImagen imagen)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                string sql = @"DELETE FROM imagen WHERE id = @id";
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", imagen.Id);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
