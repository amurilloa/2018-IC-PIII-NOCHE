﻿CREATE TABLE public.imagen
(
    id serial,
    titulo text  NOT NULL,
    tipo text  NOT NULL,
    descripcion text NOT NULL,
    imagen bytea NOT NULL,
    CONSTRAINT imagen_pkey PRIMARY KEY (id)
)